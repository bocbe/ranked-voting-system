﻿<#
.SYNOPSIS
   Gets voter information from a file and computes the winner
.DESCRIPTION
   The user inputs a file that is formatted to work with this script.
   The script will perform all calculations and output the winner
.PARAMETER <paramName>
   No parameters but must know the file location
.EXAMPLE
   C:\Users\Bocbe.DCSW\Desktop\vote.txt
#>

clear
Set-StrictMode -Version latest

<# Gets file location from the user #>
$filepath = Read-Host 'Enter file path: '
$info = Get-Content $filepath

<#The first number of the first line represents the amount of votes; that value is stored#>
$voteinfo = $info[0]
$numOfVotes = [convert]::ToInt32($voteinfo[0],10)

<#The second number of the first line represents the amount of candidates; that value is stored#>
$candidateinfo = $info[0]
$numOfCandidates = [convert]::ToInt32($candidateinfo[2], 10)

<#The second line contains all the candidates; this line is stored in a array#>
$listOfCandidates = $info[1]
$candidates = $listOfCandidates.Split(" ")

<#Calculations#>
$i = 0
$j = 2
$arrayOfVotes = @()
while ($i -lt $numOfVotes)
{
    $arrayOfVotes += $info[$j]
    $j++
    $i++    
}

$i = 0
$j = 0


while ($i -lt $numOfVotes)
{
    while ($j -lt $numOfCandidates)
    {
        
        j++
    }
    $i++
}


